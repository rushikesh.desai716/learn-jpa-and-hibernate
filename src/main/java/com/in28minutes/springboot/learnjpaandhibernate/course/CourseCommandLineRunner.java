package com.in28minutes.springboot.learnjpaandhibernate.course;

import com.in28minutes.springboot.learnjpaandhibernate.course.springdatajpa.CourseSpringDataJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CourseCommandLineRunner implements CommandLineRunner {

//    @Autowired
//    private CourseJDBCRepository repository;

//    @Autowired
//    private CourseJpaRepository repository;

    @Autowired
    private CourseSpringDataJpaRepository repository;

    @Override
    public void run(String... args) throws Exception {
        repository.save(new Course(1, "Learn New AWS 2023", "Stephane"));
        repository.save(new Course(2, "Learn DevOps 2023", "Stephane"));
        repository.save(new Course(3, "Learn Spring Cloud 2023", "Ranga"));
        repository.save(new Course(4, "Learn Core Java", "Tim"));

        repository.deleteById(3L);
        System.out.println(repository.findById(1L));
        System.out.println(repository.findById(2L));
        System.out.println(repository.findAll());
        System.out.println(repository.count());
        System.out.println(repository.findByAuthor("Stephane"));
        System.out.println(repository.findByName("Learn New AWS 2023"));
    }
}
