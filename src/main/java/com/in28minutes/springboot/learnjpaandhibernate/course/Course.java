package com.in28minutes.springboot.learnjpaandhibernate.course;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

import java.util.Objects;

@Entity     //  @Entity(name = "Course_Details") --> Notation if Entity-name is different from Table-name.
public class Course {
    @Id
    private long id;
    @Column(name = "name")     //  Defining @Column is not mandatory since class-name matches to the table-name.
    private String name;
    private String author;

    public Course() {
    }

    public Course(long id, String name, String author) {
        this.id = id;
        this.name = name;
        this.author = author;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", author='" + author + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Course course))
            return false;

        return Objects.equals(this.id, course.id) &&
                Objects.equals(this.name, course.name) &&
                Objects.equals(this.author, course.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.name, this.author);
    }
}
